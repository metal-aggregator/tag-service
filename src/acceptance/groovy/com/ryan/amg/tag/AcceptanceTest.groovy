package com.ryan.amg.tag

import com.ryan.amg.tag.config.AcceptanceTestConfig
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@Configuration
@ContextConfiguration(initializers = [ConfigFileApplicationContextInitializer])
@EnableConfigurationProperties(AcceptanceTestConfig)
abstract class AcceptanceTest extends Specification {}
