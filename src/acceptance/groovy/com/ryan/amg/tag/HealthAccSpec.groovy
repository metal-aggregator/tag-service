package com.ryan.amg.tag

import com.ryan.amg.tag.config.AcceptanceTestConfig
import com.ryan.amg.tag.config.RestClientBuilder
import groovyx.net.http.RESTClient
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import spock.lang.Shared

class HealthAccSpec extends AcceptanceTest {

    private static final Log LOG = LogFactory.getLog(getClass())

    @Shared RESTClient restClient
    @Shared boolean initialized = false

    @Autowired AcceptanceTestConfig acceptanceTestConfig

    def initializeTests() {
        if (!initialized) {
            restClient = RestClientBuilder.build(acceptanceTestConfig.target.host)
            initialized = true
        }
    }

    def setup() {
        initializeTests()

        LOG.info('===========================================================================================================================================================')
        LOG.info("Initiating test: ${specificationContext.currentIteration.name}")
        LOG.info('===========================================================================================================================================================')
    }

    def cleanup() {
        LOG.info('===========================================================================================================================================================')
        LOG.info("Completing test: ${specificationContext.currentIteration.name}")
        LOG.info('===========================================================================================================================================================')
    }

    def "GET / Asserts that the microservice is healthy"() {

        when:
            def response = restClient.get(
                path: "/",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ]
            )
            Map health = response.data

        then:
            response.status == 200
            health.status == 'UP'

    }

}
