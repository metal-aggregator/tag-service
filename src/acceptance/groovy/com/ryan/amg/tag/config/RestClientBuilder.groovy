package com.ryan.amg.tag.config

import groovyx.net.http.RESTClient

class RestClientBuilder {

    static RESTClient build(String targetHost) {
        return new RESTClient(targetHost)
    }

}
