package com.ryan.amg.tag

import com.ryan.amg.tag.config.AcceptanceTestConfig
import com.ryan.amg.tag.config.RestClientBuilder
import groovyx.net.http.RESTClient
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import spock.lang.Shared

class TagAccSpec extends AcceptanceTest {

    private static final Log LOG = LogFactory.getLog(getClass())

    @Shared RESTClient restClient
    @Shared boolean initialized = false

    @Autowired AcceptanceTestConfig acceptanceTestConfig

    def initializeTests() {
        if (!initialized) {
            restClient = RestClientBuilder.build(acceptanceTestConfig.target.host)
            initialized = true
        }
    }

    def setup() {
        initializeTests()

        LOG.info('===========================================================================================================================================================')
        LOG.info("Initiating test: ${specificationContext.currentIteration.name}")
        LOG.info('===========================================================================================================================================================')
    }

    def cleanup() {
        LOG.info('===========================================================================================================================================================')
        LOG.info("Completing test: ${specificationContext.currentIteration.name}")
        LOG.info('===========================================================================================================================================================')
    }

    def "GET /api/tags Retrieves all existing tags"() {

        given:
            String inputTagName = 'Acc-test-tag_' + UUID.randomUUID().toString()
            String reviewId = 'ReviewId_' + UUID.randomUUID().toString()
            def tagRequest = [
                tags: [
                    inputTagName
                ],
                reviewId: reviewId
            ]

        when:
            def response = restClient.post(
                path: "/api/tags",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: tagRequest
            )

        then:
            response.status == 200

        when:
            def getAllTagsResponse = restClient.get(
                path: "/api/tags"
            )
            def actualGetAllTagsResponse = getAllTagsResponse.data

        then:
            getAllTagsResponse.status == 200
            actualGetAllTagsResponse.tags
            actualGetAllTagsResponse.tags.size() > 0
            actualGetAllTagsResponse.tags.find { it.tagName == tagRequest.tags[0] }

        cleanup:
            LOG.info('================================== Start cleanup ==================================')
            restClient.delete(path: "/api/tags/${inputTagName}")
            LOG.info('==================================  End cleanup  ==================================')

    }

    def "GET /api/tags?type=GENRE Retrieves all GENRE tags"() {

        given:
            String inputTagName1 = 'Acc-test-tag_' + UUID.randomUUID().toString()
            String inputTagName2 = 'Acc-test-tag_' + UUID.randomUUID().toString()
            String reviewId = 'ReviewId_' + UUID.randomUUID().toString()
            def tagRequest = [
                tags: [
                    inputTagName1,
                    inputTagName2
                ],
                reviewId: reviewId
            ]

        when:
            def response = restClient.post(
                path: "/api/tags",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: tagRequest
            )

        then:
            response.status == 200

        when:
            def tagTypeUpdateResponse = restClient.put(
                path: "/api/tags/${inputTagName1}/type/GENRE"
            )

        then:
            tagTypeUpdateResponse.status == 200

        when:
            def getAllTagsResponse = restClient.get(
                path: "/api/tags",
                query: [
                    'type': 'GENRE'
                ]
            )
            def actualGetAllTagsResponse = getAllTagsResponse.data

        then:
            getAllTagsResponse.status == 200
            actualGetAllTagsResponse.tags
            actualGetAllTagsResponse.tags.size() > 0
            actualGetAllTagsResponse.tags.find { it.tagName == inputTagName1 }
            !actualGetAllTagsResponse.tags.find { it.tagName == inputTagName2 }

        cleanup:
            LOG.info('================================== Start cleanup ==================================')
            restClient.delete(path: "/api/tags/${inputTagName1}")
            restClient.delete(path: "/api/tags/${inputTagName2}")
            LOG.info('==================================  End cleanup  ==================================')

    }

    def "GET /api/tags?names=name1&names=name2 Retrieves tags with matching names"() {

        given:
            String inputTagName1 = 'Acc-test-tag_' + UUID.randomUUID().toString()
            String inputTagName2 = 'Acc-test-tag_' + UUID.randomUUID().toString()
            String inputTagName3 = 'Acc-test-tag_' + UUID.randomUUID().toString()
            String reviewId = 'ReviewId_' + UUID.randomUUID().toString()
            def tagRequest = [
                tags: [
                    inputTagName1,
                    inputTagName2,
                    inputTagName3
                ],
                reviewId: reviewId
            ]

        when:
            def response = restClient.post(
                path: "/api/tags",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: tagRequest
            )

        then:
            response.status == 200

        when:
            def tagTypeUpdateResponse = restClient.put(
                path: "/api/tags/${inputTagName1}/type/GENRE"
            )

        then:
            tagTypeUpdateResponse.status == 200

        when:
            def getAllTagsResponse = restClient.get(
                path: "/api/tags",
                query: [
                    'names': [inputTagName1, inputTagName2]
                ]
            )
            def actualGetAllTagsResponse = getAllTagsResponse.data

        then:
            getAllTagsResponse.status == 200
            actualGetAllTagsResponse.tags
            actualGetAllTagsResponse.tags.size() == 2
            actualGetAllTagsResponse.tags.find { it.tagName == inputTagName1 }
            actualGetAllTagsResponse.tags.find { it.tagName == inputTagName2 }

        cleanup:
            LOG.info('================================== Start cleanup ==================================')
            restClient.delete(path: "/api/tags/${inputTagName1}")
            restClient.delete(path: "/api/tags/${inputTagName2}")
            restClient.delete(path: "/api/tags/${inputTagName3}")
            LOG.info('==================================  End cleanup  ==================================')

    }

    def "GET /api/tags?type=GENRE&names=name1&names=name2 Retrieves tags with matching names"() {

        given:
            String inputTagName1 = 'Acc-test-tag_' + UUID.randomUUID().toString()
            String inputTagName2 = 'Acc-test-tag_' + UUID.randomUUID().toString()
            String inputTagName3 = 'Acc-test-tag_' + UUID.randomUUID().toString()
            String reviewId = 'ReviewId_' + UUID.randomUUID().toString()
            def tagRequest = [
                tags: [
                    inputTagName1,
                    inputTagName2,
                    inputTagName3
                ],
                reviewId: reviewId
            ]

        when:
            def response = restClient.post(
                path: "/api/tags",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: tagRequest
            )

        then:
            response.status == 200

        when:
            def tagTypeUpdateResponse = restClient.put(
                path: "/api/tags/${inputTagName1}/type/GENRE"
            )

        then:
            tagTypeUpdateResponse.status == 200

        when:
            def getAllTagsResponse = restClient.get(
                path: "/api/tags",
                query: [
                    'names': [inputTagName1, inputTagName2],
                    'type': 'GENRE'
                ]
            )
            def actualGetAllTagsResponse = getAllTagsResponse.data

        then:
            getAllTagsResponse.status == 200
            actualGetAllTagsResponse.tags
            actualGetAllTagsResponse.tags.size() == 1
            actualGetAllTagsResponse.tags.find { it.tagName == inputTagName1 }

        cleanup:
            LOG.info('================================== Start cleanup ==================================')
            restClient.delete(path: "/api/tags/${inputTagName1}")
            restClient.delete(path: "/api/tags/${inputTagName2}")
            restClient.delete(path: "/api/tags/${inputTagName3}")
            LOG.info('==================================  End cleanup  ==================================')

    }

    def "POST /api/tags Creates a new tag when non-existent tags are passed in"() {

        given:
            String reviewId1 = 'ReviewId_' + UUID.randomUUID().toString()
            String reviewId2 = 'ReviewId_' + UUID.randomUUID().toString()
            def tagRequest1 = [
                tags: [
                    'Acc-test-tag_' + UUID.randomUUID().toString(),
                    'Acc-test-tag_' + UUID.randomUUID().toString()
                ],
                reviewId: reviewId1
            ]

            def tagRequest2 = [
                tags: tagRequest1.tags,
                reviewId: reviewId2
            ]


        when:
            def firstResponse = restClient.post(
                path: "/api/tags",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: tagRequest1
            )
            def actualFirstResponse = firstResponse.data

        then:
            firstResponse.status == 200
            actualFirstResponse.tags
            actualFirstResponse.tags.size() == 2
            actualFirstResponse.tags.find { it.tagName == tagRequest1.tags[0] }
            actualFirstResponse.tags.find { it.tagName == tagRequest1.tags[1] }
            actualFirstResponse.tags.each { tag ->
                assert tag.type == 'UNKNOWN'
                assert tag.reviewIds == [reviewId1]
            }

        when:
            def secondResponse = restClient.post(
                path: "/api/tags",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: tagRequest2
            )
            def actualSecondResponse = secondResponse.data

        then:
            secondResponse.status == 200
            actualSecondResponse.tags
            actualSecondResponse.tags.size() == 2
            actualSecondResponse.tags.find { it.tagName == tagRequest2.tags[0] }
            actualSecondResponse.tags.find { it.tagName == tagRequest2.tags[1] }
            actualSecondResponse.tags.each { tag ->
                assert tag.type == 'UNKNOWN'
                assert tag.reviewIds == [reviewId1, reviewId2]
            }

        cleanup:
            LOG.info('================================== Start cleanup ==================================')
            restClient.delete(path: "/api/tags/${tagRequest1.tags.get(0)}")
            restClient.delete(path: "/api/tags/${tagRequest1.tags.get(1)}")
            LOG.info('==================================  End cleanup  ==================================')

    }

    def "PUT /api/tags/{tagName}/type/{tagType} Updates the type of the requested tag"() {

        given:
            String inputTagName = 'Acc-test-tag_' + UUID.randomUUID().toString()
            String reviewId = 'ReviewId_' + UUID.randomUUID().toString()
            def tagRequest = [
                tags: [
                    inputTagName
                ],
                reviewId: reviewId
            ]

        when:
            def response = restClient.post(
                path: "/api/tags",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: tagRequest
            )

        then:
            response.status == 200

        when:
            def tagTypeUpdateResponse = restClient.put(
                path: "/api/tags/${inputTagName}/type/GENRE"
            )
            def actualUpdateResponse = tagTypeUpdateResponse.data

        then:
            tagTypeUpdateResponse.status == 200
            actualUpdateResponse.tagName == inputTagName
            actualUpdateResponse.type == 'GENRE'
            actualUpdateResponse.reviewIds == [reviewId]

        cleanup:
            LOG.info('================================== Start cleanup ==================================')
            restClient.delete(path: "/api/tags/${inputTagName}")
            LOG.info('==================================  End cleanup  ==================================')

    }

    def "DELETE /api/tags/{tagId} Deletes a tag when an existing tag name is provided"() {

        given:
            String inputTagName = 'Acc-test-tag_' + UUID.randomUUID().toString()
            def tagRequest = [
                tags: [
                    inputTagName
                ]
            ]

        when:
            def response = restClient.post(
                path: "/api/tags",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: tagRequest
            )

        then:
            response.status == 200

        when:
            def tagDeletionResponse = restClient.delete(
                path: "/api/tags/${inputTagName}"
            )

        then:
            tagDeletionResponse.status == 204

    }

}
