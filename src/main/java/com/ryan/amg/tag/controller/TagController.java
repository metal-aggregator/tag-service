package com.ryan.amg.tag.controller;

import com.ryan.amg.tag.domain.Tag;
import com.ryan.amg.tag.domain.TagType;
import com.ryan.amg.tag.dto.TagsCreateRequestDTO;
import com.ryan.amg.tag.dto.TagsDTO;
import com.ryan.amg.tag.service.TagService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/tags")
@AllArgsConstructor
public class TagController {

    private final TagService tagService;

    @GetMapping
    public TagsDTO getTags(@RequestParam(required = false) TagType type, @RequestParam(required = false, value = "names") List<String> tagNames) {
        return new TagsDTO(tagService.getTags(type, tagNames));
    }

    @PostMapping
    public TagsDTO createTags(@RequestBody TagsCreateRequestDTO tagsCreateRequestDTO) {
        return new TagsDTO(tagService.createTags(tagsCreateRequestDTO.getTags(), tagsCreateRequestDTO.getReviewId()));
    }

    @PutMapping(value = "{tagName}/type/{tagType}")
    public Tag updateTagType(@PathVariable String tagName, @PathVariable TagType tagType) {
        return tagService.updateTagType(tagName, tagType);
    }

    @DeleteMapping(value = "/{tagName}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTag(@PathVariable String tagName) {
        tagService.deleteTag(tagName);
    }

}
