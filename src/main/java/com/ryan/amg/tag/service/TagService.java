package com.ryan.amg.tag.service;

import com.ryan.amg.tag.db.entity.TagEntity;
import com.ryan.amg.tag.db.repository.TagRepository;
import com.ryan.amg.tag.domain.Tag;
import com.ryan.amg.tag.domain.TagType;
import com.ryan.amg.tag.exception.TagNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Component
@AllArgsConstructor
public class TagService {

    private final TagRepository tagRepository;

    public List<Tag> getTags(TagType targetType, List<String> targetNames) {
        return StreamSupport.stream(retrieveTagsByType(targetType, targetNames).spliterator(), false).map(TagEntity::toDomain).collect(Collectors.toList());
    }

    public Tag getTagByName(String tagName) {
        TagEntity tagEntity = tagRepository.findByTagName(tagName);
        if (tagEntity == null) {
            throw new TagNotFoundException(tagName);
        }
        return tagEntity.toDomain();
    }

    public List<Tag> createTags(List<String> tags, String reviewId) {
        List<Tag> existingTags = retrieveExistingTags(tags);
        List<Tag> newTags = determineNewTags(tags, existingTags);

        List<Tag> allTagsToWrite = Stream.concat(existingTags.stream(), newTags.stream()).collect(Collectors.toList());
        assignReviewToTags(allTagsToWrite, reviewId);

        List<TagEntity> entitiesToWrite = Stream.concat(existingTags.stream(), newTags.stream()).map(TagEntity::new).collect(Collectors.toList());
        List<TagEntity> writtenTags = (List<TagEntity>) tagRepository.saveAll(entitiesToWrite);

        return writtenTags.stream().map(TagEntity::toDomain).collect(Collectors.toList());
    }

    public Tag updateTagType(String tagName, TagType tagType) {
        Tag existingTag = getTagByName(tagName);
        existingTag.setType(tagType);
        return (tagRepository.save(new TagEntity(existingTag))).toDomain();
    }

    public void deleteTag(String tagName) {
        tagRepository.deleteById(tagName);
    }

    private Iterable<TagEntity> retrieveTagsByType(TagType type, List<String> names) {
        if (type == null && CollectionUtils.isEmpty(names)) {
            return tagRepository.findAll();
        } else if (type != null && CollectionUtils.isEmpty(names)) {
            return tagRepository.findByType(type);
        } else if (type == null && !CollectionUtils.isEmpty(names)) {
            return tagRepository.findByTagNameIn(names);
        } else {
            return tagRepository.findByTagNameInAndTypeEquals(names, type);
        }
    }

    private List<Tag> retrieveExistingTags(List<String> tags) {
        return ((List<TagEntity>) tagRepository.findAllById(tags)).stream().map(TagEntity::toDomain).collect(Collectors.toList());
    }

    private void assignReviewToTags(List<Tag> existingTags, String reviewId) {
        existingTags.forEach(k -> k.addReviewId(reviewId));
    }

    private List<Tag> determineNewTags(List<String> proposedTags, List<Tag> existingTags) {
        List<String> existingTagNames = existingTags.stream().map(Tag::getTagName).collect(Collectors.toList());
        return proposedTags.stream().filter(k -> !existingTagNames.contains(k)).map(Tag::new).collect(Collectors.toList());
    }

}
