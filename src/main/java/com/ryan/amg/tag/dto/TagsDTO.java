package com.ryan.amg.tag.dto;

import com.ryan.amg.tag.domain.Tag;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagsDTO {
    private List<Tag> tags;
}
