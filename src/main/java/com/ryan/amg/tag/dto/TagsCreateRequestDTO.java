package com.ryan.amg.tag.dto;

import lombok.Data;

import java.util.List;

@Data
public class TagsCreateRequestDTO {
    private List<String> tags;
    private String reviewId;
}
