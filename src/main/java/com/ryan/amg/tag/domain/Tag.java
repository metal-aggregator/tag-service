package com.ryan.amg.tag.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tag {
    private String tagName;
    private TagType type;
    private List<String> reviewIds = new ArrayList<>();

    public Tag(String tagName) {
        this.tagName = tagName;
        this.type = TagType.UNKNOWN;
    }

    public void addReviewId(String reviewId) {
        this.reviewIds.add(reviewId);
        // TODO Make this not add duplicates
    }
}
