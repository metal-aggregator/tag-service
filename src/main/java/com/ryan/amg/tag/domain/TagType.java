package com.ryan.amg.tag.domain;

public enum TagType {
    GENRE,
    BAND,
    SCORE,
    UNKNOWN,
    OTHER
}
