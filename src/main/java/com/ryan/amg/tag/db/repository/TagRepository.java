package com.ryan.amg.tag.db.repository;

import com.ryan.amg.tag.db.entity.TagEntity;
import com.ryan.amg.tag.domain.TagType;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@EnableScan
public interface TagRepository extends CrudRepository<TagEntity, String> {
    TagEntity findByTagName(String tagName);
    Iterable<TagEntity> findByType(TagType type);
    Iterable<TagEntity> findByTagNameIn(List<String> names);
    Iterable<TagEntity> findByTagNameInAndTypeEquals(List<String> names, TagType type);
}
