package com.ryan.amg.tag.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Optional;

@Configuration
@EnableDynamoDBRepositories(basePackages = "com.ryan.amg.tag.db.repository")
public class DynamoDbConfig {

    @Value("${amazon.dynamodb.endpoint:}") private String dynamoDbEndpoint;

    @Bean
    public AmazonDynamoDB amazonDynamoDB(Optional<AWSCredentialsProvider> awsCredentialsProvider) {
        AmazonDynamoDBClientBuilder clientBuilder = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(dynamoDbEndpoint, null));
        awsCredentialsProvider.ifPresent(clientBuilder::withCredentials);
        return clientBuilder.build();
    }

}
