package com.ryan.amg.tag.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("default")
public class AWSCredentialsProviderConfig {

    @Value("${amazon.aws.access-key:}") private String dynamoDbAccessKey;
    @Value("${amazon.aws.secret-key:}") private String dynamoDbSecretKey;

    @Bean
    public AWSCredentialsProvider amazonAWSCredentials() {
        return new AWSStaticCredentialsProvider(new BasicAWSCredentials(dynamoDbAccessKey, dynamoDbSecretKey));
    }

}
