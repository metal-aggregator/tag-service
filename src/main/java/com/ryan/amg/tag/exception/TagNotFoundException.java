package com.ryan.amg.tag.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TagNotFoundException extends RuntimeException {

    private static final String ERROR_MESSAGE_TEMPLATE = "The tag with tagName=%s was not found.";

    public TagNotFoundException(String tagName) {
        super(String.format(ERROR_MESSAGE_TEMPLATE, tagName));
    }
}
