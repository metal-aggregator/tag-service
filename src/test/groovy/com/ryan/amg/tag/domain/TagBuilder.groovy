package com.ryan.amg.tag.domain

import com.ryan.amg.tag.db.entity.TagEntity

class TagBuilder {
    String tagName = 'Grindcore'
    TagType type = TagType.GENRE
    List<String> reviewIds = ['rev-x1']

    TagBuilder withTagName(String tagName) {
        this.tagName = tagName
        return this
    }

    TagBuilder withType(TagType type) {
        this.type = type
        return this
    }

    TagBuilder withReviewIds(List<String> reviewIds) {
        this.reviewIds = reviewIds
        return this
    }

    TagBuilder fromTagEntity(TagEntity tagEntity) {
        this.tagName = tagEntity.tagName
        this.type = TagType.valueOf(tagEntity.type)
        this.reviewIds = tagEntity.reviewIds.collect()
        return this
    }

    Tag build() {
        return new Tag(
            tagName: tagName,
            type: type,
            reviewIds: reviewIds
        )
    }

}
