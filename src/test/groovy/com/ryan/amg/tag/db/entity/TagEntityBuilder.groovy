package com.ryan.amg.tag.db.entity

import com.ryan.amg.tag.domain.TagType

class TagEntityBuilder {
    String tagName = 'Grindcore'
    TagType type = TagType.GENRE
    List<String> reviewIds = ['reviewId-1']

    TagEntityBuilder withTagName(String tagName) {
        this.tagName = tagName
        return this
    }

    TagEntityBuilder withType(TagType type) {
        this.type = type
        return this
    }

    TagEntityBuilder withReviewIds(List<String> reviewIds) {
        this.reviewIds = reviewIds
        return this
    }

    TagEntity build() {
        return new TagEntity(
            tagName: tagName,
            type: type,
            reviewIds: reviewIds
        )
    }

}
