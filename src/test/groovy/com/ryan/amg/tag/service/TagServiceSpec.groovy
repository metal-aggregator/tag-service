package com.ryan.amg.tag.service

import com.ryan.amg.tag.db.repository.TagRepository
import com.ryan.amg.tag.domain.Tag
import com.ryan.amg.tag.domain.TagType
import com.ryan.amg.tag.db.entity.TagEntityBuilder
import com.ryan.amg.tag.domain.TagBuilder
import com.ryan.amg.tag.db.entity.TagEntity
import com.ryan.amg.tag.exception.TagNotFoundException
import org.unitils.reflectionassert.ReflectionComparatorMode
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class TagServiceSpec extends Specification {

    TagRepository mockTagRepository = Mock()

    TagService tagService = new TagService(mockTagRepository)

    def "Invoking getTags with no type or names returns all of the tags in the database"() {

        given:
            List<TagEntity> mockedTagEntities = [new TagEntityBuilder().build(), new TagEntityBuilder().withTagName('Bathory').withType(TagType.BAND).build()]
            List<Tag> expectedTags = [new TagBuilder().fromTagEntity(mockedTagEntities.get(0)).build(), new TagBuilder().fromTagEntity(mockedTagEntities.get(1)).build()]

            1 * mockTagRepository.findAll() >> mockedTagEntities

        when:
            List<Tag> actualTags = tagService.getTags(null, null)

        then:
            assertReflectionEquals(expectedTags, actualTags, ReflectionComparatorMode.LENIENT_ORDER)

    }

    def "Invoking getTags with a target type invokes the tag-specific repository method and returns the expected Tags"() {

        given:
            List<TagEntity> mockedTagEntities = [new TagEntityBuilder().build(), new TagEntityBuilder().withTagName('Bathory').withType(TagType.BAND).build()]
            List<Tag> expectedTags = [new TagBuilder().fromTagEntity(mockedTagEntities.get(0)).build(), new TagBuilder().fromTagEntity(mockedTagEntities.get(1)).build()]

            1 * mockTagRepository.findByType(TagType.UNKNOWN) >> mockedTagEntities

        when:
            List<Tag> actualTags = tagService.getTags(TagType.UNKNOWN, null)

        then:
            assertReflectionEquals(expectedTags, actualTags, ReflectionComparatorMode.LENIENT_ORDER)

    }

    def "Invoking getTags with a target name list invokes the name-list-specific repository method and returns the expected Tags"() {

        given:
            List<TagEntity> mockedTagEntities = [new TagEntityBuilder().build(), new TagEntityBuilder().withTagName('Bathory').withType(TagType.BAND).build()]
            List<Tag> expectedTags = [new TagBuilder().fromTagEntity(mockedTagEntities.get(0)).build(), new TagBuilder().fromTagEntity(mockedTagEntities.get(1)).build()]

            1 * mockTagRepository.findByTagNameIn(_ as List<String>) >> {  args ->
                assertReflectionEquals(['Bathory', 'Sodom'], (List<String>) args.get(0), ReflectionComparatorMode.LENIENT_ORDER)
                return mockedTagEntities
            }

        when:
            List<Tag> actualTags = tagService.getTags(null, ['Bathory', 'Sodom'])

        then:
            assertReflectionEquals(expectedTags, actualTags, ReflectionComparatorMode.LENIENT_ORDER)

    }

    def "Invoking getTags with a both a target name list and type invokes the conjunctive type/name repository method and returns the expected Tags"() {

        given:
            List<TagEntity> mockedTagEntities = [new TagEntityBuilder().build(), new TagEntityBuilder().withTagName('Bathory').withType(TagType.BAND).build()]
            List<Tag> expectedTags = [new TagBuilder().fromTagEntity(mockedTagEntities.get(0)).build(), new TagBuilder().fromTagEntity(mockedTagEntities.get(1)).build()]

            1 * mockTagRepository.findByTagNameInAndTypeEquals(_ as List<String>, TagType.BAND) >> {  args ->
                assertReflectionEquals(['Bathory', 'Sodom'], (List<String>) args.get(0), ReflectionComparatorMode.LENIENT_ORDER)
                return mockedTagEntities
            }

        when:
            List<Tag> actualTags = tagService.getTags(TagType.BAND, ['Bathory', 'Sodom'])

        then:
            assertReflectionEquals(expectedTags, actualTags, ReflectionComparatorMode.LENIENT_ORDER)

    }

    def "Invoking getTagByName with an existing tag name calls the repository and returns a well-formed Tag"() {

        given:
            String inputTagName = 'Doom Metal'

            TagEntity mockedTagEntity = new TagEntityBuilder().withTagName(inputTagName).withReviewIds(['reviewId-123']).build()
            Tag expectedTag = new TagBuilder().withReviewIds(['reviewId-123']).withTagName(inputTagName).build()

            1 * mockTagRepository.findByTagName(inputTagName) >> mockedTagEntity

        when:
            Tag actualTag = tagService.getTagByName(inputTagName)

        then:
            assertReflectionEquals(expectedTag, actualTag)

    }

    def "Invoking getTagByName with a non-existent tag name calls the repository and throws a TagNotFoundException"() {

        given:
            String inputTagName = 'Sludge Metal'
            1 * mockTagRepository.findByTagName(inputTagName) >> null

        when:
            tagService.getTagByName(inputTagName)

        then:
            thrown(TagNotFoundException)

    }

    def "Invoking createTags correctly filters and invokes the TagRepository to save the new tags with the assigned reviewId"() {

        given:
            List<String> inputTags = ['X', 'Y', 'Z']
            String inputReviewId = 'reviewId-123'

            List<String> expectedProposedTags = ['X', 'Y', 'Z']
            List<TagEntity> mockedExistingTagEntities = [
                new TagEntityBuilder().withTagName('Y').withType(TagType.UNKNOWN).withReviewIds([]).build(),
                new TagEntityBuilder().withTagName('Z').withType(TagType.GENRE).withReviewIds(['reviewId-456']).build()
            ]

            List<TagEntity> expectedRepositoryTagEntities = [
                new TagEntityBuilder().withTagName('X').withType(TagType.UNKNOWN).withReviewIds(['reviewId-123']).build(),
                new TagEntityBuilder().withTagName('Y').withType(TagType.UNKNOWN).withReviewIds(['reviewId-123']).build(),
                new TagEntityBuilder().withTagName('Z').withType(TagType.GENRE).withReviewIds(['reviewId-123', 'reviewId-456']).build()
            ]

            List<TagEntity> mockedRepositoryTagEntities = [
                new TagEntityBuilder().withTagName('X').withType(TagType.UNKNOWN).withReviewIds(['reviewId-123']).build(),
                new TagEntityBuilder().withTagName('Y').withType(TagType.UNKNOWN).withReviewIds(['reviewId-123']).build(),
                new TagEntityBuilder().withTagName('Z').withType(TagType.GENRE).withReviewIds(['reviewId-123', 'reviewId-456']).build()
            ]

            List<Tag> expectedFinalTags = [
                new TagBuilder().withTagName('X').withType(TagType.UNKNOWN).withReviewIds(['reviewId-123']).build(),
                new TagBuilder().withTagName('Y').withType(TagType.UNKNOWN).withReviewIds(['reviewId-123']).build(),
                new TagBuilder().withTagName('Z').withType(TagType.GENRE).withReviewIds(['reviewId-123', 'reviewId-456']).build()
            ]

            1 * mockTagRepository.findAllById(_) >> { args ->
                assertReflectionEquals(expectedProposedTags, (List<String>) args.get(0), ReflectionComparatorMode.LENIENT_ORDER)
                return mockedExistingTagEntities
            }

            1 * mockTagRepository.saveAll(_ as List<TagEntity>) >> { args ->
                assertReflectionEquals(expectedRepositoryTagEntities, (List<TagEntity>) args.get(0), ReflectionComparatorMode.LENIENT_ORDER)
                return mockedRepositoryTagEntities
            }

        when:
            List<Tag> actualCreatedTags = tagService.createTags(inputTags, inputReviewId)

        then:
            assertReflectionEquals(expectedFinalTags, actualCreatedTags, ReflectionComparatorMode.LENIENT_ORDER)

    }

    def "Invoking updateTagType updates an existing tag's type and persists it to the database"() {

        given:
            TagEntity mockedRetrievedTagEntity = new TagEntityBuilder().withType(TagType.UNKNOWN).build()
            TagEntity expectedPersistedTagEntity = new TagEntityBuilder().withType(TagType.GENRE).build()
            TagEntity mockedPersistedTagEntity = new TagEntityBuilder().withType(TagType.GENRE).build()

            Tag expectedTag = new TagBuilder().fromTagEntity(expectedPersistedTagEntity).build()

            1 * mockTagRepository.findByTagName('Grindcore') >> mockedRetrievedTagEntity
            1 * mockTagRepository.save(_ as TagEntity) >> { args ->
                assertReflectionEquals(expectedPersistedTagEntity, (TagEntity) args.get(0))
                return mockedPersistedTagEntity
            }

        when:
            Tag actualTag = tagService.updateTagType('Grindcore', TagType.GENRE)

        then:
            assertReflectionEquals(expectedTag, actualTag)

    }

    def "Invoking updateTagType throws a TagNotFoundException when the specified tag name is not found"() {

        String inputTagName = 'Gore Metal'
        1 * mockTagRepository.findByTagName(inputTagName) >> null

        when:
            tagService.updateTagType(inputTagName, TagType.SCORE)

        then:
            thrown(TagNotFoundException)

    }

    def "Invoking deleteTag calls the repository with the input tag name"() {

        given:
            String inputTagName = 'inputTagName'
            1 * mockTagRepository.deleteById(inputTagName)

        when:
            tagService.deleteTag(inputTagName)

        then:
            noExceptionThrown()

    }

}
