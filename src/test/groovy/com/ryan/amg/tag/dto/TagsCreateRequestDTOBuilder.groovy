package com.ryan.amg.tag.dto

class TagsCreateRequestDTOBuilder {

    List<String> tags = ['Death Metal', 'Speed Metal']
    String reviewId = 'reviewId-123'


    TagsCreateRequestDTOBuilder withTags(List<String> tags) {
        this.tags = tags
        return this
    }

    TagsCreateRequestDTOBuilder withReviewId(String reviewId) {
        this.reviewId = reviewId
        return this
    }

    TagsCreateRequestDTO build() {
        return new TagsCreateRequestDTO(
            tags: tags,
            reviewId: reviewId
        )
    }

}
