package com.ryan.amg.tag.dto

import com.ryan.amg.tag.domain.Tag
import com.ryan.amg.tag.domain.TagBuilder

class TagsDTOBuilder {

    List<Tag> tags = [new TagBuilder().build()]

    TagsDTOBuilder withTags(List<Tag> tags) {
        this.tags = tags
        return this
    }

    TagsDTO build() {
        return new TagsDTO(
            tags: tags
        )
    }

}
