package com.ryan.amg.tag.controller

import com.ryan.amg.tag.domain.Tag
import com.ryan.amg.tag.domain.TagBuilder
import com.ryan.amg.tag.domain.TagType
import com.ryan.amg.tag.dto.TagsCreateRequestDTO
import com.ryan.amg.tag.dto.TagsCreateRequestDTOBuilder
import com.ryan.amg.tag.dto.TagsDTO
import com.ryan.amg.tag.dto.TagsDTOBuilder
import com.ryan.amg.tag.service.TagService
import org.unitils.reflectionassert.ReflectionComparatorMode
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class TagControllerSpec extends Specification {

    TagService mockTagService = Mock()

    TagController tagController = new TagController(mockTagService)

    def "Invoking getTags calls the service layer and returns the expected list of tags"() {

        given:
            List<Tag> mockedTags = [new TagBuilder().withTagName('Dissection').withType(TagType.BAND).build(), new TagBuilder().withTagName('Black Metal').withType(TagType.GENRE).build()]
            TagsDTO expectedTagsDTO = new TagsDTO([new TagBuilder().withTagName('Dissection').withType(TagType.BAND).build(), new TagBuilder().withTagName('Black Metal').withType(TagType.GENRE).build()])

            1 * mockTagService.getTags(TagType.BAND, _ as List<String>) >> { args ->
                assertReflectionEquals(['Dissection', 'Emperor'], (List<String>) args.get(1))
                return mockedTags
            }

        when:
            TagsDTO actualTags = tagController.getTags(TagType.BAND, ['Dissection', 'Emperor'])

        then:
            assertReflectionEquals(expectedTagsDTO, actualTags, ReflectionComparatorMode.LENIENT_ORDER)

    }

    def "Invoking createTags calls the service layer with the input tag names and returns the expected DTO"() {

        given:
            TagsCreateRequestDTO inputTagsCreateRequestDTO = new TagsCreateRequestDTOBuilder().withTags(['Punk', 'Avant Garde']).build()
            List<String> expectedTags = ['Punk', 'Avant Garde']
            List<Tag> mockTags = [new TagBuilder().withTagName('Punk').build(), new TagBuilder().withTagName('Avant Garde').build()]
            TagsDTO expectedTagsDTO = new TagsDTOBuilder().withTags([new TagBuilder().withTagName('Punk').build(), new TagBuilder().withTagName('Avant Garde').build()]).build()

            1 * mockTagService.createTags(_ as List<String>, 'reviewId-123') >> { args ->
                assertReflectionEquals(expectedTags, (List<String>) args.get(0))
                return mockTags
            }

        when:
            TagsDTO actualTagsDTO = tagController.createTags(inputTagsCreateRequestDTO)

        then:
            assertReflectionEquals(expectedTagsDTO, actualTagsDTO)

    }

    def "Invoking updateTagType calls the service layer with the input tag name and type"() {

        given:
            String inputTagName = 'Psychadelic Rock'
            TagType inputTagType = TagType.SCORE

            Tag mockedTag = new TagBuilder().build()
            Tag expectedTag = new TagBuilder().build()

            1 * mockTagService.updateTagType(inputTagName, inputTagType) >> mockedTag

        when:
            Tag actualTag = tagController.updateTagType(inputTagName, inputTagType)

        then:
            assertReflectionEquals(expectedTag, actualTag)

    }

    def "Invoking deleteTag calls the service layer with the input tag name"() {

        given:
            String inputTagName = 'Occult Rock'

            1 * mockTagService.deleteTag(inputTagName)

        when:
            tagController.deleteTag(inputTagName)

        then:
            noExceptionThrown()

    }

}
