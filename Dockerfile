FROM openjdk:11
COPY ./build/libs/* ./opt/tag-service/lib/app.jar
WORKDIR /opt/tag-service/lib
ENTRYPOINT ["/usr/local/openjdk-11/bin/java"]
CMD ["-jar", "app.jar"]
EXPOSE 6660
